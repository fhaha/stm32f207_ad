#include "stm32f2xx.h"
#include "usart.h"
#include "systick.h"
#include "stm32f2xx_tim.h"

void Tim2_Init(void)
{
	TIM_TimeBaseInitTypeDef  timInitStruct;
	TIM_OCInitTypeDef 	 TIM_OCInitStructure;
	//设置IO口 
	GPIO_InitTypeDef GPIO_InitStructure; 
	//RCC_APB1PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);  
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0;  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;     
	GPIO_Init(GPIOA, &GPIO_InitStructure); 
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource0,GPIO_AF_TIM2); 
	
	
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_3; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;  
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; 	
	GPIO_Init(GPIOB, &GPIO_InitStructure); 
	GPIO_PinAFConfig(GPIOB,GPIO_PinSource3,GPIO_AF_TIM2); 

	
	  
	//设置定时器1  
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);  
	//重新将Timer设置为缺省值  
	TIM_DeInit(TIM2);  
	//采用内部时钟给TIM1提供时钟源  
	TIM_InternalClockConfig(TIM2);  
	  
	timInitStruct.TIM_ClockDivision = TIM_CKD_DIV4;               
	timInitStruct.TIM_Prescaler = 300 - 1;                            //计数  
	timInitStruct.TIM_CounterMode = TIM_CounterMode_Up;         //向上计数  
	//timInitStruct.TIM_RepetitionCounter = 0;  
	timInitStruct.TIM_Period = 2000 - 1 ;                             //这个值实际上就是TIMX->ARR，延时开始时重新设定即可  
	  
	TIM_TimeBaseInit(TIM2, &timInitStruct);  
	  
	//设置PWM输出  
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;  
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;  
	TIM_OCInitStructure.TIM_Pulse = 64;  
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;  
	TIM_OC1Init(TIM2, &TIM_OCInitStructure);  
	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable); 
	
		//ARR预装载缓冲器使能  
	TIM_ARRPreloadConfig(TIM2, ENABLE);
	
	
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing; 
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; 
	TIM_OCInitStructure.TIM_Pulse = 50; 
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; 
	TIM_OC2Init(TIM2, &TIM_OCInitStructure);  
	TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable); 
	
	TIM_ITConfig(TIM2,TIM_IT_CC2,ENABLE);
	//开启定时器  
	TIM_Cmd(TIM2, ENABLE);  
	  
 
}
